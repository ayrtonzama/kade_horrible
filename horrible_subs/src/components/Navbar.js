import React from "react";
import { Navbar } from "react-bootstrap";
import { Nav } from "react-bootstrap";
import { NavDropdown } from "react-bootstrap";

const NavB = props => {
  return (
    <div>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Navbar.Brand className="brand" href="#home">
          HorribleSubs
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="responsive-navbar-nav"
          className="navItem"
        />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="ml-auto navItem">
            <Nav.Link href="#shows">SHOWS</Nav.Link>
            <Nav.Link href="#current">CURRENT SEASON</Nav.Link>
            <Nav.Link href="#schedule">SCHEDULE</Nav.Link>
            <NavDropdown title="IRC" id="collasible-nav-dropdown">
              <NavDropdown.Item href="#guide">Guide</NavDropdown.Item>
              <NavDropdown.Item href="#channel">Channel</NavDropdown.Item>
              <NavDropdown.Item href="#packlist">Packlist</NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="#faq">FAQ</Nav.Link>
            <Nav.Link href="#follow">FOLLOW US</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
    /*
    <nav className="navbar navbar-light bg-dark">
      <a className="navbar-brand" href="/">
        HorribleSubs
      </a>
      <ul className="nav justify-content-end">
        <li className="nav-item">
          <NavLink to="/" className="nav-link">
            SHOWS
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/" className="nav-link">
            CURRENT SEASON
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/" className="nav-link">
            SCHEDULE
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/" className="nav-link">
            IRC
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/" className="nav-link">
            FAQ
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/" className="nav-link">
            FOLLOW US
          </NavLink>
        </li>
      </ul>
    </nav>*/
  );
};

export default NavB;
